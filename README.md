# README #

### What is this repository for? ###

* Power Shell Scripts for automating the creation and set up of a cloud accessible VM's (azure) based on a master windows image.
* 1.0


### How do I get set up? ###

The "ServerList.txt" File contains the lists of server names to be added or removed.


To add VM(s):

1. Edit the "ServerList.txt" File with the desired VM name(s). Each VM name should be on a new line.
2. Right click on "AddVM.ps1" and select Run with power shell.



To remove/delete VM(s):

1.Edit the "ServerList.txt" File with the desired VM name(s). (Warning this will remove all trace on the VM(s) in the "ServerList.txt" File
2.Each VM name should be on a new line.


### Other Commands: ###

Open a power shell window and type the following:

List VM(s) and current Status: Get-AzureVM
Restart VM: Restart-AzureVM -ServiceName "VM Service Name" -Name "VM Machine Name"