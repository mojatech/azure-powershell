Import-Module Azure
Get-Content c:\scripts\ServerList.txt | Foreach-Object {Remove-AzureDeployment -ServiceName $_ -Slot Production -Force -DeleteVHD}
Get-Content c:\scripts\ServerList.txt | Foreach-Object {Remove-AzureService -ServiceName $_ -Force}