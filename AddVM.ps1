Import-Module Azure
$image = "Stream-Capture" 
$pwd = "Swarm0nline1"
$un = "Paul"

Get-Content c:\scripts\ServerList.txt | Foreach-Object {New-AzureVMConfig -Name $_ -InstanceSize "Small" -Image $image | Add-AzureProvisioningConfig -Windows -AdminUserName $un -Password $pwd -DisableAutomaticUpdates | Add-AzureEndpoint -Name 'PSList' -LocalPort 445 -PublicPort 445 -Protocol tcp | Set-AzureEndpoint -Name "RemoteDesktop" -LocalPort 3389 -PublicPort 13389 -Protocol tcp | New-AzureVM -ServiceName $_ -AffinityGroup "SwamTesting"}